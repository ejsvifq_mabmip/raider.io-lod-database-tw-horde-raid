## Interface: 80205
## Author: cqwrteur
## Title: Raider.IO LOD Database TW Horde Raid
## Version: @project-version@
## LoadOnDemand: 1
## X-RAIDER-IO-LOD: 4
## X-RAIDER-IO-LOD-FACTION: Horde

db/db_raiding_tw_horde_characters.lua
db/db_raiding_tw_horde_lookup.lua
